# KiCad Schematic & Footprint Library

Source:

1. fab academy: <https://gitlab.fabcloud.org/pub/libraries/kicad>
2. digikey: <https://www.digikey.com/en/resources/design-tools/kicad>
3. custom: self-made components
